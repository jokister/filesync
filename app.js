var fs = require('fs');
var exec = require('child_process').exec;
var gulp = require('gulp');
var clean = require('gulp-clean');
var chokidar = require('chokidar');
var runSequence = require('gulp-run-sequence');
var path = require('path');
var ncp = require('ncp').ncp;

// CONFIG:

const EXEC_COMMAND = 'cd /var/www/baseline && ./node_modules/gulp/bin/gulp.js build';
const OUTPUT = '/var/www/baseline';
const OUTPUT_FILES = [
	'/var/www/baseline/static',
	'/var/www/baseline/templates',
	'/var/www/baseline/src',
	'/var/www/baseline/locale'	
];
const INPUT = '/root/Dropbox';
const DEBOUNCE_TIME = 500;

// TASKS:

var watcher = chokidar.watch(INPUT);

gulp.task('removeOld', del(OUTPUT_FILES));

gulp.task('copyNew', cb => {
	console.log('copyNew');
	ncp(INPUT, OUTPUT, function (err) {
	  if (err) {
	    console.error(err);
	    return cb(err);
	  }
	  console.log('Copying files complete.');
	  return cb();
	});
});

gulp.task('compile', cb => {
	exec(EXEC_COMMAND, function(err) {
		if (err) return cb(err);
		return cb();
	});
});

// MAIN CODE:

gulp.task('root', function (cb) {
    runSequence('removeOld', 'copyNew', 'compile', cb);
});

fs.watch(INPUT, debounce(watcherCallback, DEBOUNCE_TIME));

// FUNCTIONS:

function watcherCallback(path, stats) {
	console.log('File', path, 'changed size to', stats);
	runSequence('root');
}

function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

function del(src) {
    return end => {
    	console.log('removeOld');
        return gulp.src(src, { read: false })
            .pipe(clean({force: true}));
    }
};
